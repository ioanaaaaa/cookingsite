function displayText(id) {
  let x = document.getElementById(id);
  x.style.display = 'block';
}

function closeText(id) {
  let x = document.getElementById(id);
  x.style.display = 'none';
}

window.onload = function () {
  var canvas = document.getElementById("myCanvas");
  var ctx = canvas.getContext("2d");
  ctx.font = "30px Arial";
  ctx.strokeText("CookingSite", 0, 50);
  setInterval(function(){canvas.style.webkitAnimationIterationCount = "4";
},10000);
};