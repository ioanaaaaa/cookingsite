var myIndex = 0;
var carouselInterval;
var carouselElement = document.getElementById("carousel");
var carouselNextElement = document.getElementById("carouselNextElement");
var carouselPrevElement = document.getElementById("carouselPrevElement");
var carouselSlideInterval = 1000;

function stopCarousel() {
	clearInterval(carouselInterval);
}

function startCarousel() {
	carouselInterval = setInterval(nextCarouselFrame, carouselSlideInterval);
}

function nextCarouselFrame() {
  var i;
  var x = document.getElementsByClassName("mySlides");
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";
  }
  myIndex++;
  if (myIndex > x.length) { myIndex = 1 }
  x[myIndex - 1].style.display = "block";
}

function prevCarouselFrame() {
  var i;
  var x = document.getElementsByClassName("mySlides");
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";
  }
  myIndex--;
  if (myIndex < 1) { myIndex = x.length }
  x[myIndex - 1].style.display = "block";
}

function changeTextColor(id) {
  var x = document.getElementsByClassName(id);
  for (let i = 0; i < x.length; i++) {
    x[i].style.color = 'pink';
  }
}

let elements = document.getElementsByClassName("menu_title");
for (let i = 0; i < elements.length; i++) {
  elements[i].addEventListener("mouseover", changeTextColor, false);
}

window.onload = function () {
  var canvas = document.getElementById("myCanvas");
  var ctx = canvas.getContext("2d");
  ctx.font = "30px Arial";
  ctx.strokeText("CookingSite", 0, 50);
  setInterval(function(){
	  canvas.style.webkitAnimationIterationCount = "4";
	},10000);
};

startCarousel();
carouselElement.addEventListener("mouseover", stopCarousel, false);
carouselElement.addEventListener("mouseout", startCarousel, false);
carouselNextElement.addEventListener("click", nextCarouselFrame);
carouselPrevElement.addEventListener("click", prevCarouselFrame);
document.onkeydown = checkKey;

function checkKey(e) {

    e = e || window.event;
    if (e.keyCode == '37') {
       prevCarouselFrame();
    }
    else if (e.keyCode == '39') {
       nextCarouselFrame();
    }

}

